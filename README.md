# Mini App

Mini App project for Multiplayer Online Game Design
Hosea Nathaniel R
4210181012

![](GIF/Hosea_MiniApp.gif)

Start couroutine


    `  private void Start()
    {
    getJsonData();
    }
    public void getJsonData()
    {
    StartCoroutine(RequestWebService());
    }`

Input web URL


    ` string getDataURL = "https://5e510330f2c0d300147c034c.mockapi.io/users";
    print(getDataURL);`

Request dan ambil data dari URL


    `using(UnityWebRequest webData = UnityWebRequest.Get(getDataURL))
    {
    yield return webData.SendWebRequest();
    if(webData.isNetworkError || webData.isHttpError)
    {
    print("ERROR");
    print(webData.error);
    }`

Parse JSON agar bisa dibaca


    `if (webData.isDone)
    {
    JSONNode jsondata = JSON.Parse(System.Text.Encoding.UTF8.GetString(webData.downloadHandler.data));
    if(jsondata == null)`

Show data sebagai text UI


    `else
    {
    print("JSON DATA");
    print("jsondata.Count + jsondata.Count");
    stinNama = jsondata[11]["name"];
    stinEmail = jsondata[11]["email"];
    textinti.text = "Hello, " + stinNama + " your email is " + stinEmail;
    karakter.GetComponent<Animator>().Play("Scene");
    }`
