﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;

public class TesAmbilJason : MonoBehaviour
{
    public string stinNama, stinEmail;
    public Text textinti;
    public GameObject karakter;

    private void Start()
    {
        getJsonData();
    }
    public void getJsonData()
    {
        StartCoroutine(RequestWebService());
    }

    IEnumerator RequestWebService()
    {
        string getDataURL = "https://5e510330f2c0d300147c034c.mockapi.io/users";
        print(getDataURL);

        using(UnityWebRequest webData = UnityWebRequest.Get(getDataURL))
        {
            yield return webData.SendWebRequest();
            if(webData.isNetworkError || webData.isHttpError)
            {
                print("ERROR");
                print(webData.error);
            }
            else
            {
                if (webData.isDone)
                {
                    JSONNode jsondata = JSON.Parse(System.Text.Encoding.UTF8.GetString(webData.downloadHandler.data));
                    if(jsondata == null)
                    {
                        print("NO DATA");
                    }
                    else
                    {
                        print("JSON DATA");
                        print("jsondata.Count + jsondata.Count");
                        stinNama = jsondata[11]["name"];
                        stinEmail = jsondata[11]["email"];
                        textinti.text = "Hello, " + stinNama + " your email is " + stinEmail;
                        karakter.GetComponent<Animator>().Play("Scene");
                    }
                }
            }
        }
    }
}
